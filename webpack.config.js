const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const devMode = !process.env.NODE_ENV || process.env.NODE_ENV !== 'production';

let response = {
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, 'dist/static'),
    filename: 'javascript/bundle.js',
    publicPath: '/static/',
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: "babel-loader",
            options: {
              configFile: "./babel.config.js",
              cacheDirectory: true
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          devMode
            ? 'style-loader'
            : MiniCssExtractPlugin.loader,
          'css-loader'
        ],
      },
      {
        test: /\.(png|jpg)$/,
        loader: devMode
          ? 'url-loader' :
          'file-loader',
        options: {
          name: '[path][name].[ext]'
        }
      }
    ]
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "resources/logo.png", to: "resources" }
      ],
    })
  ],
  mode: devMode ? 'development' : 'production',
  devServer: {
    inline: true
  },
  devtool: 'source-map',
  optimization: {
    minimize: true,
    minimizer: [
      new CssMinimizerPlugin()
    ]
  },
};

if (!devMode) {
  response = {
    ...response,
    plugins: [
      ...response.plugins,
      new MiniCssExtractPlugin({
        filename: 'styles/main.css'
      }),
      new HtmlWebpackPlugin({
        template: 'index.html',
        filename: '../index.html',
        inject: false
      })
    ]
  };
}

module.exports = response;
