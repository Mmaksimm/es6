import { showModal } from './modal'
import { createFighterImage } from '../fighterPreview';
import App from '../../app'

export function showWinnerModal(fighter) {
  // call showModal function 
  const bodyElement = createFighterImage(fighter)
  bodyElement.classList.remove("fighter-preview___img")

  showModal({
    title: `${fighter.name}'s winned! `,
    bodyElement,
    onClose: () => {
      document.getElementById('root').innerHTML = '';
      new App();
    }
  })
}
