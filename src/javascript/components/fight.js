import { controls } from '../../constants/controls';
import Player from './player';
import { handlerKeyDown, handlerKeyUp } from '../helpers/pressKeyHelpers';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const players = {
      playerLeft: new Player(
        firstFighter,
        'left',
        controls.PlayerOneBlock,
        controls.PlayerOneAttack,
        controls.PlayerOneCriticalHitCombination
      ),
      playerRight: new Player(
        secondFighter,
        'right',
        controls.PlayerTwoBlock,
        controls.PlayerTwoAttack,
        controls.PlayerTwoCriticalHitCombination
      )
    }

    const handlerForKeyDown = async (event) => {
      const winner = await handlerKeyDown({ event, players });
      if (winner) {
        document.removeEventListener('keydown', handlerForKeyDown);
        document.removeEventListener('keyup', handlerKeyUp);
        resolve(winner);
      };
    }

    document.addEventListener('keydown', handlerForKeyDown);
    document.addEventListener('keyup', (event) => handlerKeyUp({ event, players }));
  });
}

export function getDamage(attacker, defender = null) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;

  return attack * criticalHitChance
}


export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;

  return defense * dodgeChance;
}
