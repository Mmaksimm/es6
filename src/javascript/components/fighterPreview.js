import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right'
    ? 'fighter-preview___right'
    : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)
  if (!fighter) return;
  const fighterImage = createFighterImage(fighter);
  const fighterInfo = createInfo(fighter);

  fighterElement.append(fighterImage, fighterInfo);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createInfo(fighter) {
  const fighterInfo = createElement({ tagName: 'pre', className: 'fighter-preview__name' });
  fighterInfo.textContent = Object.keys(fighter).map(key => {
    if (key === '_id' || key === 'source') return;
    return `${key}: ${fighter[key]} \n`;
  }
  ).filter(el => el).join('');

  return fighterInfo;
}