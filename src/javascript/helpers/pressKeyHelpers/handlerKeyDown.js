import pressKeyHandler from './pressKeyHandler';
import { getDamage } from '../../components/fight';

export function handlerKeyDown({ event, players }) {
  event.preventDefault();
  const { playerPressKey = false, playerNotPressKey = false } = pressKeyHandler({ event, players });
  if (!playerPressKey) return;
  if (playerPressKey.block || playerPressKey.attack) return;

  const key = playerPressKey.fighterControlKeyNameFind(event);

  const damageСounting = damage => {
    if (playerNotPressKey.calculationAndRenderHalthBar(damage)) {
      return playerPressKey.fighter
    };
  };

  if (key.name === 'criticalHitCombination') {
    if (playerPressKey.criticalHitCombinationTest(event)) return;
    playerPressKey.keyDownCriticalHitCombination(event);

    if (playerPressKey.criticalHitCombinationNotAllowed) return;
    if (!playerPressKey.criticalHitCombinationOk()) return;

    playerPressKey.criticalHitCombinationNotAllowedSet();
    const damage = 2 * playerPressKey.fighter.attack;
    return damageСounting(damage);
  };

  if (playerPressKey.criticalHitCombination.some(key => key.value)) return;

  if (key.name === 'block') {
    playerPressKey.keyDownBlock();
  };

  if (key.name === 'attack') {
    playerPressKey.keyDownAttack();

    if (playerNotPressKey.block) return;

    const damage = getDamage(
      playerPressKey.fighter,
      playerNotPressKey.fighter
    );

    if (damage) return damageСounting(damage);
  };

}