import pressKeyHandler from './pressKeyHandler';

export function handlerKeyUp({ event, players }) {
  event.preventDefault();

  const { playerPressKey = false } = pressKeyHandler({ event, players });
  if (!playerPressKey) return;

  const key = playerPressKey.fighterControlKeyNameFind(event);

  if (key.name === 'criticalHitCombination') {
    playerPressKey.keyUpCriticalHitCombination(event);
  };

  if (key.name === 'block') {
    playerPressKey.keyUpBlock();
  };

  if (key.name === 'attack') {
    playerPressKey.keyUpAttack();
  };
}