export default function pressKeyHandler({ event, players }) {
  if (players.playerLeft.fighterControlKeyTest(event)) {
    return {
      playerPressKey: players['playerLeft'],
      playerNotPressKey: players['playerRight']
    }
  } else if (players.playerRight.fighterControlKeyTest(event)) {
    return {
      playerPressKey: players['playerRight'],
      playerNotPressKey: players['playerLeft']
    }
  } else {
    return {};
  };
}